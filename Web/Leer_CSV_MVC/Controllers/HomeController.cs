﻿using Leer_CSV_MVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Leer_CSV_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        public ActionResult Index()
        { 
            return View(new List<Personas>());
        }


        [HttpPost]
        public ActionResult Index(HttpPostedFileBase postedFile)
        {
            List<Personas> personas = new List<Personas>();
            string filePath = string.Empty;



            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                string csvData = System.IO.File.ReadAllText(filePath);
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        personas.Add(new Personas
                        {
                            Nombre = row.Split(';')[0],
                            Edad = Convert.ToInt32(row.Split(';')[1]),
                            Equipo = row.Split(';')[2],
                            EstadoCivil = row.Split(';')[3],
                            NivelEstudio = row.Split(';')[4]




                        });
                    }

                    ViewBag.Total = personas.Count();

                    ViewBag.Promedio = personas.Average(x => x.Edad);

                    ViewBag.Top100 = personas.Add({ { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { { } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } } })

                }
            }
            return View(personas);
          
        }

        [HttpPost]
        public ActionResult Promedio(HttpPostedFileBase postedFile1)
        {
            List<Personas> person = new List<Personas>();
            string filePath = string.Empty;



            if (postedFile1 != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                filePath = path + Path.GetFileName(postedFile1.FileName);
                string extension = Path.GetExtension(postedFile1.FileName);
                postedFile1.SaveAs(filePath);

                string csvData = System.IO.File.ReadAllText(filePath);

                List<int> total = (from d in person
                             where d.Equipo == "Racing"
                             orderby d.Edad
                             select d.Edad).ToList();
                
                var promedio = total.Average();
                ViewBag.Promedio = total;

            }

                return View();
            
        }



        [HttpPost]
        public ActionResult Top100(HttpPostedFileBase postedFile)
        {
            List<Personas> person = new List<Personas>();
            string filePath = string.Empty;



            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                string csvData = System.IO.File.ReadAllText(filePath);

                List<string> top = (from d in person
                                    orderby d.Edad
                                    select d.Nombre + " " + d.Edad + " " + d.Equipo).ToList();

                    var top100 = top.Take(100).ToList();
                    ViewBag.Top100 = top;
                }
            return View(person);

            }

        
    
        public ActionResult Top100()
        {
            return View(new List<Personas>());
        }

    
    
    }

    }
